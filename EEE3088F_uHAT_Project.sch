EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title "Raspberry Pi Zero (W) uHAT Template Board"
Date "2019-02-28"
Rev "1.0"
Comp ""
Comment1 "This Schematic is licensed under MIT Open Source License."
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR0101
U 1 1 5C777805
P 4850 4100
F 0 "#PWR0101" H 4850 3850 50  0001 C CNN
F 1 "GND" H 4855 3927 50  0001 C CNN
F 2 "" H 4850 4100 50  0001 C CNN
F 3 "" H 4850 4100 50  0001 C CNN
	1    4850 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5C777838
P 5750 4100
F 0 "#PWR0102" H 5750 3850 50  0001 C CNN
F 1 "GND" H 5755 3927 50  0001 C CNN
F 2 "" H 5750 4100 50  0001 C CNN
F 3 "" H 5750 4100 50  0001 C CNN
	1    5750 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5C778504
P 4450 4200
F 0 "#PWR0105" H 4450 3950 50  0001 C CNN
F 1 "GND" H 4455 4027 50  0001 C CNN
F 2 "" H 4450 4200 50  0001 C CNN
F 3 "" H 4450 4200 50  0001 C CNN
	1    4450 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 4150 4450 4200
$Comp
L Mechanical:MountingHole H1
U 1 1 5C7C4C81
P 8250 2600
F 0 "H1" H 8350 2646 50  0000 L CNN
F 1 "MountingHole" H 8350 2555 50  0000 L CNN
F 2 "lib:MountingHole_2.7mm_M2.5_uHAT_RPi" H 8250 2600 50  0001 C CNN
F 3 "~" H 8250 2600 50  0001 C CNN
	1    8250 2600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5C7C7FBC
P 8250 2800
F 0 "H2" H 8350 2846 50  0000 L CNN
F 1 "MountingHole" H 8350 2755 50  0000 L CNN
F 2 "lib:MountingHole_2.7mm_M2.5_uHAT_RPi" H 8250 2800 50  0001 C CNN
F 3 "~" H 8250 2800 50  0001 C CNN
	1    8250 2800
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5C7C8014
P 8250 3000
F 0 "H3" H 8350 3046 50  0000 L CNN
F 1 "MountingHole" H 8350 2955 50  0000 L CNN
F 2 "lib:MountingHole_2.7mm_M2.5_uHAT_RPi" H 8250 3000 50  0001 C CNN
F 3 "~" H 8250 3000 50  0001 C CNN
	1    8250 3000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5C7C8030
P 8250 3200
F 0 "H4" H 8350 3246 50  0000 L CNN
F 1 "MountingHole" H 8350 3155 50  0000 L CNN
F 2 "lib:MountingHole_2.7mm_M2.5_uHAT_RPi" H 8250 3200 50  0001 C CNN
F 3 "~" H 8250 3200 50  0001 C CNN
	1    8250 3200
	1    0    0    -1  
$EndComp
$Sheet
S 3100 4450 1550 1550
U 60BA0CAA
F0 "STATUS LED CIRCUIT" 50
F1 "STATUS_LED_CIRCUIT.sch" 50
$EndSheet
$Sheet
S 5200 4450 1500 1550
U 60BA0E7D
F0 "REGULATOR CIRCUIT" 50
F1 "REGULATOR_CIRCUIT.sch" 50
$EndSheet
$Sheet
S 7250 4450 1350 1600
U 60BA0F06
F0 "AMPLIFIER CIRCUIT" 50
F1 "AMPLIFIER_CIRCUIT.sch" 50
$EndSheet
Text GLabel 8400 3600 0    50   Input ~ 0
24V
$Comp
L Device:Battery BT1
U 1 1 60BAF9B7
P 8550 3800
F 0 "BT1" H 8658 3846 50  0000 L CNN
F 1 "24V" H 8658 3755 50  0000 L CNN
F 2 "Battery:Battery_Panasonic_CR1025-VSK_Vertical_CircularHoles" V 8550 3860 50  0001 C CNN
F 3 "~" V 8550 3860 50  0001 C CNN
	1    8550 3800
	1    0    0    -1  
$EndComp
Text GLabel 8450 4000 0    50   Input ~ 0
GND
Wire Wire Line
	8450 4000 8550 4000
Wire Wire Line
	8550 3600 8400 3600
$Comp
L Connector:Conn_01x40_Male J2
U 1 1 60BB4FBC
P 1300 3950
F 0 "J2" H 1408 6031 50  0000 C CNN
F 1 "Conn_01x40_Male" H 1408 5940 50  0000 C CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_2x20_P1.00mm_Vertical" H 1300 3950 50  0001 C CNN
F 3 "~" H 1300 3950 50  0001 C CNN
	1    1300 3950
	1    0    0    -1  
$EndComp
Text GLabel 1750 2350 2    50   Input ~ 0
5V
Wire Wire Line
	1750 2350 1700 2350
Wire Wire Line
	1500 2150 1700 2150
Wire Wire Line
	1700 2150 1700 2350
Connection ~ 1700 2350
Wire Wire Line
	1700 2350 1500 2350
Text GLabel 1700 3650 2    50   Input ~ 0
3V3
Text GLabel 1700 2050 2    50   Input ~ 0
3V3
Wire Wire Line
	1700 2050 1500 2050
Wire Wire Line
	1700 3650 1500 3650
Text GLabel 1700 5850 2    50   Input ~ 0
GND
Wire Wire Line
	1700 5850 1600 5850
Wire Wire Line
	1600 5850 1600 5350
Wire Wire Line
	1600 4450 1500 4450
Connection ~ 1600 5850
Wire Wire Line
	1600 5850 1500 5850
Wire Wire Line
	1500 4950 1600 4950
Connection ~ 1600 4950
Wire Wire Line
	1600 4950 1600 4450
Wire Wire Line
	1500 5350 1600 5350
Connection ~ 1600 5350
Wire Wire Line
	1600 5350 1600 4950
Wire Wire Line
	1500 3350 2000 3350
Wire Wire Line
	2000 3350 2000 3950
Wire Wire Line
	2000 3950 1600 3950
Wire Wire Line
	1600 3950 1600 4450
Connection ~ 1600 3950
Wire Wire Line
	1600 3950 1500 3950
Connection ~ 1600 4450
Wire Wire Line
	1500 2550 2000 2550
Wire Wire Line
	2000 2550 2000 2850
Connection ~ 2000 3350
Wire Wire Line
	1500 2850 2000 2850
Connection ~ 2000 2850
Wire Wire Line
	2000 2850 2000 3350
NoConn ~ 1500 2250
NoConn ~ 1500 2450
NoConn ~ 1500 2650
NoConn ~ 1500 2750
NoConn ~ 1500 2950
NoConn ~ 1500 3050
NoConn ~ 1500 3150
NoConn ~ 1500 3250
NoConn ~ 1500 3450
NoConn ~ 1500 3550
NoConn ~ 1500 3750
NoConn ~ 1500 3850
NoConn ~ 1500 4050
NoConn ~ 1500 4150
NoConn ~ 1500 4250
NoConn ~ 1500 4350
NoConn ~ 1500 4550
NoConn ~ 1500 4650
NoConn ~ 1500 4750
NoConn ~ 1500 4850
NoConn ~ 1500 5050
NoConn ~ 1500 5150
NoConn ~ 1500 5250
NoConn ~ 1500 5450
NoConn ~ 1500 5550
NoConn ~ 1500 5650
NoConn ~ 1500 5750
NoConn ~ 1500 5950
$EndSCHEMATC
